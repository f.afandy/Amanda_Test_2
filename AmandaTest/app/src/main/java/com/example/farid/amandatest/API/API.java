package com.example.farid.amandatest.API;

import  com.example.farid.amandatest.models.Models;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.converter.gson.GsonConverterFactory;

public interface API {
    String BASE_URL2 = "http://api.teknorial.com/";

    @GET("contohjson") Call<API> getData();

    class Factory{
        private static API service;
        public static API getInstance(){

            if (service == null){
                Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_URL2)
                        .build();

                service = retrofit.create(API.class);
                return service;
            }else{
                return  service;
            }

        }
    }

    //@GET("/contohjson")
    //Call<Models> getDataAdmin();
}
