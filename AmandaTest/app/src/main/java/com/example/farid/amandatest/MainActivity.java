package com.example.farid.amandatest;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.app.ProgressDialog;
import android.widget.Toast;

import com.example.farid.amandatest.API.GithubUser;
import com.example.farid.amandatest.API.GithubUserAPI;
import com.example.farid.amandatest.data.model.Weather;
import com.example.farid.amandatest.data.remote.WeatherAPI;
import com.example.farid.amandatest.models.Models;
import com.example.farid.amandatest.API.API;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.view.View.OnClickListener;

import android.content.Intent;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    String status = "Status Android: ";
    public static final String ROOT_URL = "http://api.teknorial.com";


    private  TextView txt_id;
    private  TextView txt_nama;
    private  TextView txt_email;
    private  TextView txt_alamat;
    private  TextView txt_status;

    @BindView(R.id.txt_Temp) TextView txt_Temp;
    @BindView(R.id.btn_listbuku) Button btn_listbuku;


    @Override

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        final Intent pindah= new Intent(this, DetailActivity.class);
        btn_listbuku.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(pindah);
            }
        });*/


        ButterKnife.bind(this);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.);
        getSupportActionBar().setTitle("F & B Recipes");
        getSupportActionBar().setSubtitle("Amanda Test");

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_icon_menu);

        TextView textView = (TextView)findViewById(R.id.textView);


        txt_id = (TextView) findViewById(R.id.txt_id);
        txt_nama = (TextView) findViewById(R.id.txt_nama);
        txt_email = (TextView) findViewById(R.id.txt_email);
        txt_alamat =(TextView) findViewById(R.id.txt_alamat);
        txt_status = (TextView) findViewById(R.id.txt_status);




        Log.d(status,"event onCreate");

        //getData();
    }


    @OnClick(R.id.btn_refresh) public void onClick_btn_Refresh(){
        final ProgressDialog loading = ProgressDialog.show(this, "Get Data","Please wait..",false,false);

        WeatherAPI.Factory.getInstance().getWeather().enqueue(new Callback<Weather>() {

            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                loading.dismiss();
                txt_Temp.setText(response.body().getQuery().getResults().getChannel().getItem().getCondition().getTemp());
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                Log.e("Failed",t.getMessage());
            }
        });
    }

    @OnClick(R.id.btn_load) public void onClick_btn_Load(){
        final ProgressDialog loading = ProgressDialog.show(this, "Get Data","Please wait..",false,false);
        API.Factory.getInstance().getData().enqueue(new Callback<API>() {
            @Override
            public void onResponse(Call<API> call, Response<API> response) {
                loading.dismiss();
            }

            @Override
            public void onFailure(Call<API> call, Throwable t) {
                Log.e("Failed",t.getMessage());
            }
        });
    }

    @OnClick(R.id.btn_listbuku) public void onClick_btn_ListBuku(){
        final Intent pindah= new Intent(this, DetailActivity.class);
        btn_listbuku.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(pindah);
            }
        });
    }
/*
    private static API service;
    private void getData() {
        final ProgressDialog loading = ProgressDialog.show(this, "Get Data","Please wait..",false,false);

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ROOT_URL)
                .build();
        service = retrofit.create(API.class);


        Call<Models> call = service.getDataAdmin();

        call.enqueue(new Callback<Models>(){
            @Override
            public void onResponse(Call<Models> call, Response<Models> response) {
                try {
                    //if (loading!= null) {
                        loading.dismiss();
                    //}

                    String id = response.body().getAdmin().getId().toString();
                    String nama = response.body().getAdmin().getNama();
                    String email = response.body().getAdmin().getEmail();
                    String alamat = response.body().getAdmin().getAlamat();
                    String status = response.body().getAdmin().getStatus();

                    txt_id.setText("ID : "+ id);
                    txt_nama.setText("Nama : "+nama);
                    txt_email.setText("Email : "+email);
                    txt_alamat.setText("Alamat : "+alamat);
                    txt_status.setText("Status : "+status );

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<Models> call, Throwable t) {

            }
        }
        );


    }
*/
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                TextView textView = (TextView)findViewById(R.id.textView);
                textView.setText("Hasil pencarian Query :" + query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(status,"event onStart");
    }

    @Override
    protected void onResume(){
        super.onResume();
        //onClick_btn_Refresh();
        Log.d(status,"event onResume");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d(status,"event onPause");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d(status,"event onStop");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        Log.d(status,"event onDestroy");
    }



}
